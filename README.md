<!--
[![Analogy Log Viewer](https://github-readme-stats.vercel.app/api/pin/?username=Analogy-LogViewer&repo=Analogy.LogViewer)](https://github.com/Analogy-LogViewer/Analogy.LogViewer)
-->
<a href="https://www.linkedin.com/in/liorbanai" target="_blank"><img alt="LinkedIn" src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" /> <a href="https://github.com//LiorBanai" target="_blank"><img alt="Github" src="https://img.shields.io/badge/GitHub-%2312100E.svg?&style=for-the-badge&logo=Github&logoColor=white" /></a>  </a> <a href="https://dev.to/liorbanai"><img src="https://img.shields.io/badge/DEV.TO-%230A0A0A.svg?&style=for-the-badge&logo=dev-dot-to&logoColor=white"></a> <a href="https://medium.com/@liorbanai" target="_blank"><img alt="Medium" src="https://img.shields.io/badge/medium-%2312100E.svg?&style=for-the-badge&logo=medium&logoColor=white" /></a> ![](https://estruyf-github.azurewebsites.net/api/VisitorHit?user=LiorBanai&repo=LiorBanai&countColorcountColor)

### Analytics ⚙️
<!--
![Github Languages](https://github-readme-stats.vercel.app/api/top-langs/?username=LiorBanai&layout=compact&count_private=true)
-->
![Github Statistics](https://github-readme-stats.vercel.app/api/?username=LiorBanai&count_private=true&show_icons=true)

![Github Contributions](https://github-readme-streak-stats.herokuapp.com/?user=LiorBanai&hide_border=true)

My Top Projects:
| Project   |      Description      |
|:----------|:---------------|
[Analogy Log Viewer](https://github.com/Analogy-LogViewer/Analogy.LogViewer) | A customizable Log Viewer with ability to create custom providers. Can be used with C#, C++, Python, Java and others | 
[Serilog parsers for Analogy Log Viewer](https://github.com/Analogy-LogViewer/Analogy.LogViewer.Serilog) | Serilog parsers for Analogy Log Viewer | 
[HDF5-CSharp](https://github.com/LiorBanai/HDF5-CSharp)| Set of tools that help in reading and writing hdf5 files for .net environments | 
 [PythonNetWrapper](https://github.com/LiorBanai/PythonNetWrapper) | a library for executing pythonnet in C# projects | 
 [Github Notifier](https://github.com/LiorBanai/GitHub-Notifier) | a small program that sits in the tray bar and periodically check repositories for activity |
 [Popup Notification Windows](https://github.com/LiorBanai/Notification-Popup-Window) | A notification window that appears on the lower right part of the screen |
 [EDF-CSharp](https://github.com/LiorBanai/EDF) | Library to read and write EDF signal files | 
 [SPE](https://github.com/LiorBanai/SPE) | 2005 University 4th Year project | 


<!--
**LiorBanai/LiorBanai** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
